# MI Docker Workshop Tag 2

## Einfaches HTTP Load Balancing mit docker-compose

### Motivation

Klassische Client-Server-Architekturen in einem Docker Laufzeitumgebung 
bestehen z.B. aus einem Applikations- und einem Datenbankcontainer. 
HTTP Anfragen (z.B. Aufruf einer Webseite) werden vom Host Server direkt 
an den Applikationscontainer weitergegeben und von diesem Verarbeitet.

![Architektur ohne Load Balancing](no_lb.png)

Für manche Applikationen kann dies einen Flaschenhals darstellen. Wenn 
z.B. die Verarbeitung einer einzelnen HTTP Anfrage den Applikationscontainer 
für längere Zeit blockiert, können währenddessen keine weiteren Anfragen 
verarbeitet werden, selbst wenn die (CPU/RAM) Ressourcen des Host Servers 
nur geringfügig ausgelastet sind.

Load Balancing schaltet in der Architektur eine Komponente vor die (mehreren) 
Applikationscontainer, die eingehende Anfragen auf diese verteilen kann.

![Architektur mit Load Balancing](lb.png)

`docker-compose` als einfaches Container-Orchestrierungswerkzeug (auf einem 
einzelnen Host Server) kann nativ Applikationscontainer horizontal skalieren.

D.h. ein in `docker-compose.yml` spezifizierter Server "application" kann mit 
einem einfachen Befehl mehrfach kopiert und die unabhängigen Instanzen gestarten werden:

```bash
docker-compose up -d --scale application=4
```

In diesem Beispiel würden vier Docker Container auf Basis des für den Service 
"application" festgelegten Images und sämtlicher weiterer Konfiguration ausgerollt 
und gestartet.

Damit einkommende Anfragen auf diese vier Instanzen korrekt verteilt werden können, 
muss zusätzlicher Aufwand betrieben werden.

### Nutzung von `jwilder/nginx-proxy` als Load Balancer

Eine denkbar einfache Lösung für Load Balancing einer docker-compose Umgebung ist das 
Nginx Reverse Proxy Image von Jason Wilder: 
[`jwilder/nginx-proxy`](https://hub.docker.com/r/jwilder/nginx-proxy)

Das Reverse Proxy Image kann nicht nur als einfacher HTTP- und TLS-Proxy genutzt werden, 
sondern übernimmt automatisch das Load Balancing, wenn die eingestellten Services mit 
docker-compose skaliert werden.

Das Vorgehen wird mit den folgenden Übungsaufgaben vorgestellt:

### Übungsaufgabe

1. Einfache REST Applikation mit Python Flask implementieren
2. Applikation mit `docker-compose` starten
3. Request Skript erstellen, um REST Anfragen zu simulieren
4. Proxy Image in docker-compose.yml einbauen

### Musterlösungen

##### 1. `git checkout origin/loadbalancing_ex1`

##### 2. `git checkout origin/loadbalancing_ex2`

##### 3. `git checkout origin/loadbalancing_ex3`

##### 4. `git checkout origin/loadbalancing_ex4`
