# MI Docker Tutorial

Eine Einführung in die Arbeit mit Docker und docker-compose für 
Mitarbeiter\*innen des 
[Instituts für Medizinische Informatik](https://medizininformatik.umg.eu) 
der Universitätsmedizin Göttingen.

* Tag 1: 1. Juli 2020, Grundlagen vorgestellt von Christian Bauer, Theresa Bender 
und Marcel Paricak
  * [Vortragsfolien](2020_Docker-Tutorial Tag 1.pdf)
  * [Übung 1](lesson1/)
  * [Übung 2](lesson1/)
  * [Übung 3](lesson1/)
  * [Vollständiger Code](lessons_learned/) der erstellten Anwendung
* Tag 2: 22. Juli 2020, weiterführende Themen vorgestellt von Christian Bauer, 
Theresa Bender, Marcel Paricak und Markus Suhr
  * [Vortragsfolien](2020_Docker-Tutorial Tag 2.pdf)
  * [Load Balancing Übung](load_balancing/README.md)
